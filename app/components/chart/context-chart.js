import Ember from 'ember';

export default Ember.Component.extend({
  tagName: 'svg',
  classNames: ['context-chart'],
  attributeBindings: ['width','height'],

  // ---------------------------------------------------------------------------
  // Drawing Functions
  // ---------------------------------------------------------------------------

  update() {
    this.updateRows();
  },

  updateRows() {
    //  check if we have inserted element already
    if(!this.get('element')) {
      return;
    }

    const xScale = d3.scale.linear().range([0, 500]).domain([0, 100]);

    const rowsData = this.get('data');

    const d3el = d3.select(this.get('element')).select('.chart-wrapper');

    const rowsSelection = d3el.selectAll('.rope-row-item').data(rowsData, function(d) {
      return d.id;
    });
    const enterRowsSelection = rowsSelection.enter();

    //  rows
    const enterRows = enterRowsSelection.append('g').attr('class', 'rope-row-item');
    enterRows
      .append('g')
        .attr('class', 'row-item__title')
        .attr('transform', 'translate(0,15)')
          .append('text')
            .text(function(d) { return d.name; });

    //  parent container for dots and line
    enterRows
      .append('g')
        .attr('class', 'row-item__group')
        .attr('transform', 'translate(100,0)');

    //draw dots
    const enterItemGroup = enterRows.select('.row-item__group'),
          radius = 11,
          yConst = 10;

    //TODO dry it with miso d3.chart
    //draw lines
    enterItemGroup
      .append('line')
      .attr('class', 'context-legend-item__line')
      .attr("x1",function (d,i) {
        let
          arr = [d.var1.value, d.var2.value, d.var3.value, d.var4.value],
          min = Math.min( ...arr );

        return xScale(min);
      })
      .attr("y1", yConst)
      .attr("x2",function (d,i) {
        let
          arr = [d.var1.value, d.var2.value, d.var3.value, d.var4.value],
          max = Math.max( ...arr );

        return xScale(max);
      })
      .attr("y2",yConst);

    enterItemGroup
            .append('svg:circle')
            .attr('class', 'context-legend-item--var1')
            .attr('cx', function (d,i) {
              return xScale(d.var1.value);
            } )
            .attr('cy', function (d) { return yConst } )
            .attr('r', radius)
            .on('mouseover', function(d){
              d3.select(this).classed('active', true);
            })
            .on('mouseout', function(d){
              d3.select(this).classed('active', false);
            });

    enterItemGroup
        .append('svg:circle')
          .attr('class', 'context-legend-item--var2')
          .attr('cx', function (d,i) {
            return xScale(d.var2.value);
           })
          .attr('cy', function (d) { return yConst } )
          .attr('r', radius)
          .on('mouseover', function(d){
            d3.select(this).classed('active', true);
          })
          .on('mouseout', function(d){
            d3.select(this).classed('active', false);
          });

    enterItemGroup
      .append('svg:circle')
        .attr('class', 'context-legend-item--var3')
        .attr('cx', function (d,i) {
          return xScale(d.var3.value);
        })
        .attr('cy', yConst )
        .attr('r', radius)
        .on('mouseover', function(d){
          d3.select(this).classed('active', true);
        })
        .on('mouseout', function(d){
          d3.select(this).classed('active', false);
        });

    enterItemGroup
      .append("svg:circle")
        .attr('class', 'context-legend-item--var4')
        .attr("cx", function (d,i) {
          return xScale(d.var4.value);
        })
        .attr("cy", yConst )
        .attr("r", radius)
        .on('mouseover', function(d){
          d3.select(this).classed('active', true);
        })
        .on('mouseout', function(d){
          d3.select(this).classed('active', false);
        });

    $('.cf div').on('mouseenter', function(ev){
      let selector = '.context-legend-item--' + $(ev.target).text().replace(/(\r\n|\n|\r)/gm,'');
      let array = [];
      d3.selectAll(selector).classed('active', true);
      d3.selectAll(selector).each( function(d, i){

        let x = parseInt(d3.select(this).attr("cx")) + 100,
            y = i * 50 + 11;

        array.push([x, y]);
      });

        let valueLine = d3.svg.line()
          .x(function(d, i) { return d[0]; })
          .y(function(d, i) { return d[1]; });

      enterRowsSelection.append("path")
        .attr("class", "context-legend-item__scatter")
        .attr("d", valueLine(array));
    });

    $('.cf div').on('mouseleave', function(ev){
      let selector = '.context-legend-item--' + $(ev.target).text().replace(/(\r\n|\n|\r)/gm,'');
      d3.selectAll(selector).classed('active', false);
      d3.selectAll('.context-legend-item__scatter').remove();
    });

    //  sort rows by
    rowsSelection
      .attr('transform', function(d,i) {
        const rowHeight = 50;
        const offsetHeight = rowHeight * i;
        return 'translate(0,' + offsetHeight + ')';
      });
  },

  didInsertElement() {
    this.update();
  },

  didReceiveAttrs() {
    this.update();
  }

});
