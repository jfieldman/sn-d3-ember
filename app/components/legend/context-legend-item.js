import Ember from 'ember';

export default Ember.Component.extend({
  tagName: 'li',
  classNames: ['context-legend-item'],
  'on-click': '',
  actions: {
    sort: function(newSort) {
      this.get('on-click')({
        sortedByParam: newSort
      });
    }
  }
});
