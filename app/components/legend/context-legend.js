import Ember from 'ember';

export default Ember.Component.extend({
  classNames: ['context-legend'],
  getData: function(){
    return this.get('data');
  },

  getValuesArray: function(data, param) {
    let arr = Object.keys( data ).map(function ( param ) { return data[param]; });
    let min = Math.min.apply( null, arr );
    let max = Math.max.apply( null, arr );
    console.log(min, max);
  },

  actions: {
  sortVarValue: function(params) {
    let sort = params.sortedByParam;
    let data = this.getData();
    let sorted;

    sorted = Ember.computed.sort('data', 'sort:desc');
    console.log(sorted);
  }
}
});
