# Context-chart

A simple Ember app template with stub of a custom chart

## Assignment

Here's what we would like you to do

### D3 part
* For each row, create dots displaying var1, var2, var3, var4 values from dataset already bound to the rows, and using xScale already created in app/components/chart/context-chart.js (see screenshot at _templates/basic.png)
* For each row, display line connecting the dots (see screenshot at _templates/basic.png)

### Ember part
* By clicking on of the legend items at the top, the chart rows get sorted by that variable (e.g. clicking ‘var4’ at the top rearranges rows so that row with the highest ‘var4’ value is at the top)

### Optional part, (aka you’re bored, this is super simple or you want to impress us)

* By hovering on of the legend item at the top, the corresponding dots get highlighted (see advanced.png screenshot)
* Draw lines that connects all dots of display value of the same variable across lines.

There are some styles at app/styles/app.css, you can reuse those. Or not.


## Prerequisites

You will need the following things properly installed on your computer.

* [Git](http://git-scm.com/)
* [Node.js](http://nodejs.org/) (with NPM)
* [Bower](http://bower.io/)
* [Ember CLI](http://www.ember-cli.com/)
* [PhantomJS](http://phantomjs.org/)

## Installation

* `git clone <repository-url>` this repository
* change into the new directory
* `npm install`
* `bower install`

## Running / Development

* `ember server`
* Visit your app at [http://localhost:4200](http://localhost:4200).

### Code Generators

Make use of the many generators for code, try `ember help generate` for more details

### Running Tests

* `ember test`
* `ember test --server`

### Building

* `ember build` (development)
* `ember build --environment production` (production)

### Deploying

I prefer the push to deploy mechanism you have with AWS, google cloud or Heroku which sometimes need fiddling to get it to work 

You can use Ember CLI deploy for it:
Ember CLI Deploy works by executing functions that have been registered with your application’s deploy pipeline. You add new functionality by installing plugins provided by the community - or writing your own. Plugins are special Ember CLI addons that autoregister themselves with your pipeline.

```sh
# Ensure Ember CLI Deploy itself is installed
ember install ember-cli-deploy

# Install the Build plugin, which builds your app during deployment
ember install ember-cli-deploy-build

# Gzip our files
ember install ember-cli-deploy-gzip

# Install the S3 plugin, to upload our app to S3
ember install ember-cli-deploy-s3
```

Sometimes, plugins require configuration. In this example, we need to give the S3 plugin our security credentials, and tell it which region and bucket to upload our app to:

```js
// config/deploy.js
ENV.s3 = {
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  bucket: 'my-ember-app',
  region: 'us-east-1'
};
```

Now, we can deploy our application. The default command deploys to the `production` target:

```sh
ember deploy
```
You can also specify a specific deploy target:
```sh
ember deploy staging
```







## Further Reading / Useful Links

* [ember.js](http://emberjs.com/)
* [ember-cli](http://www.ember-cli.com/)
* Development Browser Extensions
  * [ember inspector for chrome](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi)
  * [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)

